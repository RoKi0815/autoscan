Public Class frm_info
    Private Sub frm_Info_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btn_Abort.Select()
        Me.lbl_Version.Text = My.Application.Info.Version.ToString
    End Sub

    Private Sub btn_Abort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Abort.Click
        Me.Close()
    End Sub

End Class