﻿Public Class ListViewComparer

    Implements IComparer

    Private Column As Integer
    Private SortOrder As Windows.Forms.SortOrder

    Public Sub New(ByVal Column As Integer, ByVal SortOrder As Windows.Forms.SortOrder)

        Me.Column = Column
        Me.SortOrder = SortOrder

    End Sub

    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

        Dim item_x As ListViewItem = DirectCast(x, ListViewItem)
        Dim item_y As ListViewItem = DirectCast(y, ListViewItem)

        ' Get the sub-item values.
        Dim string_x As String
        If item_x.SubItems.Count <= Column Then
            string_x = ""
        Else
            string_x = item_x.SubItems(Column).Text
        End If

        Dim string_y As String
        If item_y.SubItems.Count <= Column Then
            string_y = ""
        Else
            string_y = item_y.SubItems(Column).Text
        End If

        'sort them
        If Me.SortOrder = Windows.Forms.SortOrder.Ascending Then
            If IsNumeric(string_x) And IsNumeric(string_y) Then
                Return Val(string_x).CompareTo(Val(string_y))
            ElseIf IsDate(string_x) And IsDate(string_y) Then
                Return DateTime.Parse(string_x).CompareTo(DateTime.Parse(string_y))
            Else
                Return String.Compare(string_x, string_y)
            End If
        Else
            If IsNumeric(string_x) And IsNumeric(string_y) Then
                Return Val(string_y).CompareTo(Val(string_x))
            ElseIf IsDate(string_x) And IsDate(string_y) Then
                Return DateTime.Parse(string_y).CompareTo(DateTime.Parse(string_x))
            Else
                Return String.Compare(string_y, string_x)
            End If
        End If

    End Function

End Class