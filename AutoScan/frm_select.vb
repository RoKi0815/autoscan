﻿Imports System.Windows.Forms

Public Class frm_select

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        'If MsgBox("Sollen die Änderungen gespeichert werden?", MsgBoxStyle.OkCancel Or MsgBoxStyle.Question, "Speichern") = MsgBoxResult.Cancel Then
        '    Exit Sub
        'End If

        Dim sControlersNew As String = ""

        For Each Item As ListViewItem In lv_control.Items

            If Item.Checked = True Then
                sControlersNew = sControlersNew & Item.SubItems(1).Text & ","
            End If

        Next

        If frm_main.tb_name.ReadOnly = False Then
            frm_main.tb_controlers.Text = frm_main.fct_remove_wrong_komma(sControlersNew)
        End If

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click

        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()

    End Sub

    Private Sub frm_select_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Height = frm_main.Height

        Dim location = Me.Location

        location.X = frm_main.Location.X + frm_main.Width
        location.Y = frm_main.Location.Y

        Me.Location = location

        lv_control.Items.Clear()
        lv_control.Columns.Clear()

        Dim ColumnName As String = ""

        ColumnName = "   "
        lv_control.Columns.Add(ColumnName, ColumnName)
        lv_control.Columns(ColumnName).Width = 22
        lv_control.Columns(ColumnName).TextAlign = HorizontalAlignment.Center

        ColumnName = "Nr"
        lv_control.Columns.Add(ColumnName, ColumnName)
        lv_control.Columns(ColumnName).Width = 40
        lv_control.Columns(ColumnName).TextAlign = HorizontalAlignment.Center

        ColumnName = "Name"
        lv_control.Columns.Add(ColumnName, ColumnName)
        lv_control.Columns(ColumnName).Width = 218
        lv_control.Columns(ColumnName).TextAlign = HorizontalAlignment.Left


        If fct_read_controlers() = False Then
            MsgBox("Fehler beim lesen der Steuergeräte", MsgBoxStyle.Exclamation, "Fehler")
            Exit Sub
        End If

        fct_get_current_controlers()

    End Sub

    Private Function fct_read_controlers() As Boolean

        Dim sConfigFile As String = System.Windows.Forms.Application.StartupPath & "\data\config.xml"

        Dim sControlers As New List(Of String)

        Try
            Dim xml As System.Xml.XmlDocument
            xml = New System.Xml.XmlDocument()
            xml.Load(sConfigFile)


            For Each Node As System.Xml.XmlNode In xml.DocumentElement

                Dim NodeName = Node.Name

                If NodeName.Contains("ControlerNumb") Then
                    If Node.HasChildNodes Then
                        sControlers.Add(Node.FirstChild.Value & ";" & Node.NextSibling.FirstChild.Value)
                    End If
                End If

            Next


            'In LV eintragen
            For Each Controler In sControlers
                Dim newItem As ListViewItem = New ListViewItem(New String() {"", Split(Controler, ";")(0), Split(Controler, ";")(1)})
                Dim iIndex As Integer = lv_control.Items.Add(newItem).Index
            Next

            lv_control.Sorting = Windows.Forms.SortOrder.Ascending
            lv_control.ListViewItemSorter = New ListViewComparer(1, Windows.Forms.SortOrder.Ascending)

            Return True

        Catch ex As Exception

            Return True

        End Try

    End Function

    Private Function fct_get_current_controlers() As Boolean

        Dim sControlersCurrent() As String = Split(frm_main.tb_controlers.Text, ",")

        For Each Controler In sControlersCurrent

            Dim isAvailable As Boolean = False

            For Each Item As ListViewItem In lv_control.Items

                If Item.SubItems(1).Text = Controler Then
                    isAvailable = True
                    Item.Checked = True
                End If

            Next

            'Wenn nicht in der Lsite, dann hinzufügen
            If isAvailable = False And Controler <> "" Then

                Dim newItem As ListViewItem = New ListViewItem(New String() {"", Controler, "???"})
                Dim iIndex As Integer = lv_control.Items.Add(newItem).Index
                lv_control.Items(iIndex).Checked = True
                lv_control.Items(iIndex).BackColor = Color.OrangeRed

            End If

        Next

        Return True

    End Function

End Class
