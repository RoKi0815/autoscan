﻿Imports System.IO

Public Class frm_main

    Dim sLCodePath As String = ""
    Dim sEDC15CalcPath As String = ""
    Dim sCSVfilePath As String = ""
    Dim bReadFIN As Boolean = False

    Dim sFile As String = ""
    Dim rowNumber As Integer = 0

    Dim isNewEntry As Boolean = False

    Private Sub frm_main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        fct_read_config()

        fct_init()

        lbl_vers.Text = "v" & My.Application.Info.Version.ToString

        'Dim location = Me.Location
        'Dim screensize = My.Computer.Screen.WorkingArea

        'Dim X As Integer = CInt(Math.Round(screensize.Width / 2 - Me.Width / 2) - Me.Width / 4)
        'Dim Y As Integer = CInt(Math.Round(screensize.Height / 2 - Me.Height / 2))

        'location.X = X
        'location.Y = Y

        'Me.Location = location

    End Sub

    Private Function fct_read_config() As Boolean

        Dim sConfigFile As String = System.Windows.Forms.Application.StartupPath & "\data\config.xml"

        Dim sControlers As New List(Of String)

        Try
            Dim xml As System.Xml.XmlDocument
            xml = New System.Xml.XmlDocument()
            xml.Load(sConfigFile)

            sLCodePath = xml.DocumentElement.SelectSingleNode("Paths").SelectSingleNode("LCode").ChildNodes(0).Value
            sEDC15CalcPath = xml.DocumentElement.SelectSingleNode("Paths").SelectSingleNode("EDC15-Calc").ChildNodes(0).Value
            sCSVfilePath = xml.DocumentElement.SelectSingleNode("Paths").SelectSingleNode("CSVfiles").ChildNodes(0).Value
            Dim stmp As String
            stmp = xml.DocumentElement.SelectSingleNode("Options").SelectSingleNode("readFIN").ChildNodes(0).Value
            If (stmp = "true") Then
                bReadFIN = True
            Else
                bReadFIN = False
            End If

            Return True

        Catch ex As Exception

            Return True

        End Try

    End Function

    Private Sub fct_init()

        lv_data.Items.Clear()
        lv_data.Columns.Clear()

        Dim ColumnName As String = ""

        ColumnName = "Dummy"
        lv_data.Columns.Add(ColumnName, ColumnName)
        lv_data.Columns(ColumnName).Width = 0
        lv_data.Columns(ColumnName).TextAlign = HorizontalAlignment.Center

        ColumnName = "Nr"
        lv_data.Columns.Add(ColumnName, ColumnName)
        lv_data.Columns(ColumnName).Width = 40
        lv_data.Columns(ColumnName).TextAlign = HorizontalAlignment.Center

        ColumnName = "Bezeichnung"
        lv_data.Columns.Add(ColumnName, ColumnName)
        lv_data.Columns(ColumnName).Width = 120
        lv_data.Columns(ColumnName).TextAlign = HorizontalAlignment.Left
        ColumnName = "Marke"
        lv_data.Columns.Add(ColumnName, ColumnName)
        lv_data.Columns(ColumnName).Width = 0
        lv_data.Columns(ColumnName).TextAlign = HorizontalAlignment.Left
        ColumnName = "Modell"
        lv_data.Columns.Add(ColumnName, ColumnName)
        lv_data.Columns(ColumnName).Width = 0
        lv_data.Columns(ColumnName).TextAlign = HorizontalAlignment.Left
        ColumnName = "kW"
        lv_data.Columns.Add(ColumnName, ColumnName)
        lv_data.Columns(ColumnName).Width = 0
        lv_data.Columns(ColumnName).TextAlign = HorizontalAlignment.Left

        ColumnName = "Steuergeräte"
        lv_data.Columns.Add(ColumnName, ColumnName)
        lv_data.Columns(ColumnName).Width = 310
        lv_data.Columns(ColumnName).TextAlign = HorizontalAlignment.Left

        ColumnName = "FIN"
        lv_data.Columns.Add(ColumnName, ColumnName)
        lv_data.Columns(ColumnName).Width = 160
        lv_data.Columns(ColumnName).TextAlign = HorizontalAlignment.Left
        ColumnName = "MKB"
        lv_data.Columns.Add(ColumnName, ColumnName)
        lv_data.Columns(ColumnName).Width = 0
        lv_data.Columns(ColumnName).TextAlign = HorizontalAlignment.Left
        ColumnName = "GKB"
        lv_data.Columns.Add(ColumnName, ColumnName)
        lv_data.Columns(ColumnName).Width = 0
        lv_data.Columns(ColumnName).TextAlign = HorizontalAlignment.Left

        lv_data.FullRowSelect = True
        lv_data.GridLines = True
        lv_data.MultiSelect = False
        lv_data.ShowItemToolTips = True

        tb_controlers.Text = ""
        tb_fid.Text = ""
        tb_mkb.Text = ""
        tb_gkb.Text = ""
        tb_no.Text = ""
        tb_name.Text = ""
        tb_marke.Text = ""
        tb_model.Text = ""
        tb_kw.Text = ""

        tb_controlers.ReadOnly = True
        tb_fid.ReadOnly = True
        tb_mkb.ReadOnly = True
        tb_gkb.ReadOnly = True
        tb_no.ReadOnly = True
        tb_name.ReadOnly = True
        tb_marke.ReadOnly = True
        tb_model.ReadOnly = True
        tb_kw.ReadOnly = True
    End Sub

    Function fct_open() As Boolean

        Dim dlg As New OpenFileDialog
        Dim bOK As Boolean = False

        dlg.Filter = "TXT files (*.txt)|*.txt|All files (*.*)|*.*"
        dlg.FilterIndex = 1
        dlg.RestoreDirectory = True
        dlg.DefaultExt = ".txt"
        dlg.AddExtension = True
        dlg.Title = "Datei öffnen..."
        dlg.FileName = ""
        'dlg.InitialDirectory = 

        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then

            sFile = dlg.FileName
            fct_read_file(dlg.FileName)

        End If

        Return True

    End Function

    Private Function fct_read_file(ByVal sFile As String) As Boolean

        lv_data.Items.Clear()

        Dim sReader As StreamReader = My.Computer.FileSystem.OpenTextFileReader(sFile, System.Text.Encoding.Default)
        Dim rLine As String = ""

        Dim iFirstNo As Integer = 1

        Dim sLines As New List(Of String)

        Do While sReader.EndOfStream = False

            rLine = sReader.ReadLine

            'Nur wenn nicht auskommentiert
            If rLine.Contains(";") = False Then

                ''Erste Zeile gefunden
                'If Split(rLine, ",")(0) = fct_fillZero(iFirstNo.ToString, 2) Then

                Dim iCounter As Integer = 0

                'Alle Zeilen, die aufeinanderfolgende Nummern haben
                Do While Split(rLine, ",")(0) = fct_fillZero((iFirstNo + iCounter).ToString, 2)

                    sLines.Add(rLine)

                    rLine = sReader.ReadLine
                    iCounter = iCounter + 1

                Loop

                Exit Do

                'End If

            End If

        Loop

        sReader.Close()
        sReader.Dispose()

        'CSV-file lesen
        Dim sLinesCSV As New List(Of String)
        Try
            Dim sCSVfileName As String = Split(sFile, ".txt")(0) & "_extended.csv"
            Dim sReaderCSV As StreamReader = My.Computer.FileSystem.OpenTextFileReader(sCSVfileName, System.Text.Encoding.Default)

            'Zeilen auftrennen
            sLinesCSV = Split(sReaderCSV.ReadToEnd(), vbNewLine).ToList()
            'Header entfernen
            If (sLinesCSV(0).Split(";")(0).ToLower().Trim().Equals("nr")) Then
                sLinesCSV.RemoveAt(0)
            End If
            'leere Zeilen entfernen
            sLinesCSV = sLinesCSV.Where(Function(s) Not s.Trim().Equals("")).ToList()

            sReaderCSV.Close()
            sReaderCSV.Dispose()
        Catch
            Debug.Print("no ""_extended.csv"" found")
        End Try

        'FIN-file lesen
        Dim sFINs As New List(Of String)
        If bReadFIN Then
            Try
                Dim sFINfileName As String = Split(sFile, ".txt")(0)
                sFINfileName = sFINfileName & ".fin"
                Dim sReaderFIN As StreamReader = My.Computer.FileSystem.OpenTextFileReader(sFINfileName, System.Text.Encoding.Default)

                Dim rLineFIN As String = sReaderFIN.ReadToEnd

                sReaderFIN.Close()
                sReaderFIN.Dispose()

                sFINs.AddRange(Split(rLineFIN, vbNewLine))
            Catch
                Debug.Print("no .fin found")
            End Try
        End If

        'Daten in Felder eintragen
        Dim merker As MsgBoxResult = MsgBoxResult.Abort
        For n = 0 To sLines.Count - 1

            'init Felder
            Dim sNo As String = ""
            Dim sName As String = ""
            Dim sMarke As String = ""
            Dim sModell As String = ""
            Dim sKM As String = ""
            Dim sControlers As String = ""
            Dim sFIN As String = ""
            Dim sMKB As String = ""
            Dim sGKB As String = ""

            'aus Autoscan.txt
            sNo = Split(sLines(n), ",")(0)
            sName = Split(sLines(n), ",")(1)
            sControlers = Split(sLines(n), ",")(2)

            Dim aControlers() As String = Split(sLines(n), ",")

            For nA = 3 To aControlers.Length - 1
                sControlers = sControlers & "," & aControlers(nA)
            Next

            'aus Autoscan_extended.csv
            If (sLinesCSV.Count > n) Then
                Try
                    sNo = Split(sLinesCSV(n), ";")(0)
                    sName = Split(sLinesCSV(n), ";")(1)
                    sMarke = Split(sLinesCSV(n), ";")(2)
                    sModell = Split(sLinesCSV(n), ";")(3)
                    sKM = Split(sLinesCSV(n), ";")(4)
                    sControlers = Split(sLinesCSV(n), ";")(5)
                    sFIN = Split(sLinesCSV(n), ";")(6)
                    sMKB = Split(sLinesCSV(n), ";")(7)
                    sGKB = Split(sLinesCSV(n), ";")(8)
                Catch
                    If Not (merker = MsgBoxResult.Ok) Then
                        merker = MsgBox("Fehler beim Lesen der Datei """ & Split(sFile, ".txt")(0) & "_extended.csv" & """. Es konnten einige Felder nicht gefunden werden. Nicht vorhandene Daten werden leer gelassen ", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Fehler beim Lesen")
                    End If
                End Try
            End If

            'aus Autoscan.fin
            If bReadFIN Then
                Try
                    sFIN = sFINs(n)
                Catch ex As Exception
                    sFIN = "NA"
                End Try
            End If

            Dim newItem As ListViewItem = New ListViewItem(New String() {"", sNo, sName, sMarke, sModell, sKM, sControlers, sFIN, sMKB, sGKB})
            Dim iIndex As Integer = lv_data.Items.Add(newItem).Index

        Next


        Return True

    End Function

    Function fct_fillZero(ByVal number As String, ByVal iLength As Integer) As String

        If Len(number) <> iLength Then
            For i As Integer = Len(number) To iLength - 1
                number = "0" & number
            Next
            Return number
        Else
            Return number
        End If

    End Function

    Private Sub btn_open_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_open.Click

        fct_open()

    End Sub

    Private Sub lv_data_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lv_data.DoubleClick

        isNewEntry = False

        fct_edit_entry()

    End Sub

    Private Function fct_edit_entry() As Boolean

        tb_no.ReadOnly = True
        tb_name.ReadOnly = False
        tb_marke.ReadOnly = False
        tb_model.ReadOnly = False
        tb_kw.ReadOnly = False
        tb_controlers.ReadOnly = False
        tb_fid.ReadOnly = False
        tb_mkb.ReadOnly = False
        tb_gkb.ReadOnly = False

        tb_no.Text = lv_data.SelectedItems(0).SubItems(1).Text
        tb_name.Text = lv_data.SelectedItems(0).SubItems(2).Text
        tb_marke.Text = lv_data.SelectedItems(0).SubItems(3).Text
        tb_model.Text = lv_data.SelectedItems(0).SubItems(4).Text
        tb_kw.Text = lv_data.SelectedItems(0).SubItems(5).Text
        tb_controlers.Text = lv_data.SelectedItems(0).SubItems(6).Text
        tb_fid.Text = lv_data.SelectedItems(0).SubItems(7).Text
        tb_mkb.Text = lv_data.SelectedItems(0).SubItems(8).Text
        tb_gkb.Text = lv_data.SelectedItems(0).SubItems(9).Text

        Return True

    End Function

    Private Sub btn_save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save.Click

        fct_save_entry(isNewEntry)

    End Sub

    Private Function fct_save_entry(ByVal isNew As Boolean) As Boolean

        If tb_no.Text = "" Then
            Return False
        End If

        If tb_name.Text = "" Then
            MsgBox("Es muss eine Name für das Fahrzeug angegeben sein.", MsgBoxStyle.Information, "Fehler")
            Return False
        End If

        tb_controlers.Text = fct_remove_wrong_komma(tb_controlers.Text)

        For Each Item As ListViewItem In lv_data.Items

            If Item.SubItems(1).Text = tb_no.Text Then

                Item.SubItems(2).Text = tb_name.Text
                Item.SubItems(3).Text = tb_marke.Text
                Item.SubItems(4).Text = tb_model.Text
                Item.SubItems(5).Text = tb_kw.Text
                Item.SubItems(6).Text = tb_controlers.Text
                Item.SubItems(7).Text = tb_fid.Text
                Item.SubItems(8).Text = tb_mkb.Text
                Item.SubItems(9).Text = tb_gkb.Text

            End If

        Next

        If isNew = True Then
            Dim newItem As ListViewItem = New ListViewItem(New String() {"", tb_no.Text, tb_name.Text, tb_marke.Text, tb_model.Text, tb_kw.Text, tb_controlers.Text, tb_fid.Text, tb_mkb.Text, tb_gkb.Text})
            Dim iIndex As Integer = lv_data.Items.Add(newItem).Index
        End If

        tb_no.ReadOnly = True
        tb_name.ReadOnly = True
        tb_marke.ReadOnly = True
        tb_model.ReadOnly = True
        tb_kw.ReadOnly = True
        tb_controlers.ReadOnly = True
        tb_fid.ReadOnly = True
        tb_mkb.ReadOnly = True
        tb_gkb.ReadOnly = True

        tb_no.Text = ""
        tb_name.Text = ""
        tb_marke.Text = ""
        tb_model.Text = ""
        tb_kw.Text = ""
        tb_controlers.Text = ""
        tb_fid.Text = ""
        tb_mkb.Text = ""
        tb_gkb.Text = ""

        Return True

    End Function

    Public Function fct_remove_wrong_komma(ByVal sText As String) As String

        'ev. falsches Komma hinter letztem controler entfernen
        Dim sTextNew As String = ""

        If Mid(StrReverse(sText), 1, 1) = "," Then
            sTextNew = Mid(sText, 1, sText.Length - 1)
        Else
            sTextNew = sText
        End If

        Return sTextNew

    End Function

    Private Function fct_save_file() As Boolean

        'Si-Copy
        My.Computer.FileSystem.CopyFile(sFile, sFile & ".bak", True)

        'Alle Einträge löschen und die beiden Textteile merken.
        Dim sReader As StreamReader = My.Computer.FileSystem.OpenTextFileReader(sFile, System.Text.Encoding.Default)
        Dim rLine As String = ""

        Dim iFirstNo As Integer = 1

        Dim sLinesTop As New List(Of String)
        Dim sLinesBot As String = ""

        Do While sReader.EndOfStream = False

            rLine = sReader.ReadLine

            'Nur wenn nicht auskommentiert
            If rLine.Contains(";") = False Then

                ''Erste Zeile gefunden
                'If Split(rLine, ",")(0) = fct_fillZero(iFirstNo.ToString, 2) Then

                Dim iCounter As Integer = 0

                'Alle Zeilen, die aufeinanderfolgende Nummern haben
                Do While Split(rLine, ",")(0) = fct_fillZero((iFirstNo + iCounter).ToString, 2)

                    rLine = sReader.ReadLine
                    iCounter = iCounter + 1

                Loop

                sLinesBot = sReader.ReadToEnd

                Exit Do

                'End If

            End If

            sLinesTop.Add(rLine)

        Loop

        sReader.Close()
        sReader.Dispose()



        'Die neuen Zeilen dazwischen Schreiben
        For Each Item As ListViewItem In lv_data.Items

            Dim sLineItem As String = ""
            sLineItem = Item.SubItems(1).Text & "," & Item.SubItems(2).Text & "," & Item.SubItems(6).Text

            sLinesTop.Add(sLineItem)

        Next

        ''FIN-file schreiben
        'Dim sFINfileName As String = Split(sFile, ".txt")(0)
        'sFINfileName = sFINfileName & ".fin"
        'Dim sFINs As String = ""

        'For Each Item As ListViewItem In lv_data.Items

        '    sFINs = sFINs & Item.SubItems(7).Text & vbNewLine

        'Next

        'CSV-file schreiben
        Dim sCSVfileName As String = Split(sFile, ".txt")(0) & "_extended.csv"
        Dim sCSV As String = "Nr;Bezeichnung;Marke;Model;kW;Steuergeräte;FIN;MKB;GKB" & vbNewLine

        For Each Item As ListViewItem In lv_data.Items

            sCSV = sCSV & Item.SubItems(1).Text & ";" & Item.SubItems(2).Text & ";" & Item.SubItems(3).Text & ";" & Item.SubItems(4).Text & ";" & Item.SubItems(5).Text & ";" & Item.SubItems(6).Text & ";" & Item.SubItems(7).Text & ";" & Item.SubItems(8).Text & ";" & Item.SubItems(9).Text & ";" & vbNewLine

        Next

        'Text in stream schreiben
        Dim newText As String = sLinesTop(0) & vbNewLine

        For n = 1 To sLinesTop.Count - 1
            newText = newText & sLinesTop(n) & vbNewLine
        Next

        newText = newText & sLinesBot

        newText = newText & "; edited withm Uwe seinem tool ;-)"

        If sFile <> "" Then
            My.Computer.FileSystem.WriteAllBytes(sFile, fct_String2ByteArray(newText), False)
            'My.Computer.FileSystem.WriteAllBytes(sFINfileName, fct_String2ByteArray(sFINs), False)
            My.Computer.FileSystem.WriteAllBytes(sCSVfileName, fct_String2ByteArray(sCSV), False)
        End If

        MsgBox("Die Datei wurde gespeichert.", MsgBoxStyle.Information, "Fertig")

        Return True

    End Function

    Private Function fct_String2ByteArray(ByVal sString As String) As Byte()

        Dim String_len As Long
        Dim StartPnt As Long

        String_len = Len(sString)
        Dim ByteArray(CInt(String_len) - 1) As Byte

        For StartPnt = 1 To String_len
            ByteArray(CInt(StartPnt) - 1) = CByte(Asc(Mid(sString, CInt(StartPnt), 1)))
        Next

        fct_String2ByteArray = ByteArray

    End Function

    Private Sub btn_export_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_export.Click

        fct_save_file()

    End Sub

    Private Sub btn_select_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_select.Click

        frm_select.ShowDialog()

    End Sub

    Private Sub btn_new_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_new.Click

        fct_new_entry()

    End Sub

    Private Function fct_new_entry() As Boolean

        isNewEntry = True

        If sFile = "" Then
            MsgBox("Es muss ein File geöffnet sein, damit es geändert werden kann.", MsgBoxStyle.Information, "Fehler")
            Return False
        End If

        tb_no.ReadOnly = True
        tb_name.ReadOnly = False
        tb_marke.ReadOnly = False
        tb_model.ReadOnly = False
        tb_kw.ReadOnly = False
        tb_controlers.ReadOnly = False
        tb_fid.ReadOnly = False
        tb_mkb.ReadOnly = False
        tb_gkb.ReadOnly = False

        'Nächste freie Nummer suchen
        lv_data.Sorting = Windows.Forms.SortOrder.Ascending
        lv_data.ListViewItemSorter = New ListViewComparer(1, Windows.Forms.SortOrder.Ascending)

        If lv_data.Items.Count = 0 Then
            tb_no.Text = "01"
        Else
            tb_no.Text = fct_fillZero(CStr(CInt(lv_data.Items(lv_data.Items.Count - 1).SubItems(1).Text) + 1), 2)
        End If

        tb_name.Text = ""
        tb_marke.Text = ""
        tb_model.Text = ""
        tb_kw.Text = ""
        tb_controlers.Text = ""
        tb_fid.Text = ""
        tb_mkb.Text = ""
        tb_gkb.Text = ""

        Return True
    End Function

    Private Sub renameCSV()
        Dim cnt As Long = 0

        For Each s_file In My.Computer.FileSystem.GetFiles(sCSVfilePath)
            If System.IO.Path.GetExtension(s_file).Equals("csv") Then
                If s_file.Contains("_") = False Then
                    Dim s_fileName As String = System.IO.Path.GetFileNameWithoutExtension(s_file)
                    My.Computer.FileSystem.RenameFile(s_file, s_fileName & "_" & Now.Year & fct_fillZero(CStr(Now.Month), 2) & fct_fillZero(CStr(Now.Day), 2) & "-" & fct_fillZero(CStr(Now.Hour), 2) & fct_fillZero(CStr(Now.Minute), 2) & ".CSV")
                    cnt = cnt + 1
                End If
            End If
        Next

        Call MsgBox("Es wurden " & cnt.ToString() & " Dateien umbenannt.", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "umbenennen")
    End Sub

    Private Sub btn_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown, btn_export.KeyDown, btn_new.KeyDown, btn_open.KeyDown, btn_select.KeyDown, lv_data.KeyDown, tb_controlers.KeyDown, tb_name.KeyDown, tb_no.KeyDown, tb_kw.KeyDown, tb_model.KeyDown, tb_marke.KeyDown, tb_gkb.KeyDown, tb_mkb.KeyDown

        If e.KeyValue = 112 Then
            frm_info.ShowDialog()
        End If

    End Sub

    Public Sub fct_tools_openProg(ByVal spath As String, ByVal wait As Boolean, Optional ByVal sArgs As String = "")

        Dim procStartInfo As ProcessStartInfo = Nothing

        Try
            procStartInfo = New ProcessStartInfo
            procStartInfo.FileName = spath
            procStartInfo.Verb = "Open"
            procStartInfo.Arguments = sArgs

            procStartInfo.CreateNoWindow = False
            procStartInfo.WindowStyle = ProcessWindowStyle.Normal

            'start process for the file with/from the associated application
            Dim procOpen As Process = Process.Start(procStartInfo)

            'Warten, dass process beendet ist
            If wait = True Then
                If Not procOpen Is Nothing Then
                    procOpen.WaitForExit()
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Fehler")
        End Try

    End Sub

    Private Sub LCodeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LCodeToolStripMenuItem.Click
        fct_tools_openProg(sLCodePath, False)
    End Sub

    Private Sub EDC15CalcToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDC15CalcToolStripMenuItem.Click
        fct_tools_openProg(sEDC15CalcPath, False)
    End Sub

    Private Sub RechnerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RechnerToolStripMenuItem.Click
        fct_tools_openProg("calc.exe", False)
    End Sub

    Private Sub EditorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditorToolStripMenuItem.Click
        fct_tools_openProg("notepad.exe", False)
    End Sub

    Private Sub CSVToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CSVToolStripMenuItem.Click
        renameCSV()
    End Sub

End Class
